# https://discuss.kakoune.com/t/single-command-for-grep-next-match-in-similar-buffers-lsp-make-find/1215
# https://github.com/mawww/kakoune/pull/3656
declare-option -hidden str current_grep_buffer
hook global WinDisplay \
    \*(?:grep|find|make|goto|diagnostics|symbols|cargo)\* %{
    set-option global current_grep_buffer %val{hook_param}
}

# https://discuss.kakoune.com/t/tiny-experiments-buffer-next-but-related-as-a-linked-list/1551/7
declare-option -hidden str last_tool
hook global BufCreate \
    \*(?:grep|find|make|goto|diagnostics|symbols|cargo)\* %{
    relate %opt{last_tool}
    set-option global last_tool %val{hook_param}
    evaluate-commands -save-regs 'm' %{
        set-register m %opt{modelinefmt}
        set-option buffer modelinefmt '[%opt{jumpclient}] <= '
        set-option -add buffer modelinefmt %reg{m}
    }
}

hook global BufClose \
    \*(?:grep|find|make|goto|diagnostics|symbols|cargo)\* %{
    try %{
        evaluate-commands -buffer %opt{relate_previous_buffer} relate %opt{relate_next_buffer}
    } catch %{
        evaluate-commands -buffer %opt{relate_previous_buffer} unset-option buffer relate_next_buffer
    }
    evaluate-commands %sh{
        if [ "$kak_hook_param" = "$kak_opt_last_tool" ]; then
            printf "set-option global last_tool '%s'\n" "$kak_opt_relate_next_buffer"
            printf "evaluate-commands -buffer '%s' unset-option buffer relate_previous_buffer\n" "$kak_opt_relate_next_buffer"
        fi
    }
}

define-command my-grep-next-match \
    -docstring 'Jump to the next match in a grep-like buffer' %{
    evaluate-commands -try-client %opt{jumpclient} %{
        buffer %opt{current_grep_buffer}
        execute-keys "%opt{grep_current_line}g<a-l> /^[^:]+:\d+:<ret>"
        grep-jump
        info "[↓↑=%opt{current_grep_buffer}]"
    }
    try %{ evaluate-commands -client %opt{toolsclient} %{
        buffer %opt{current_grep_buffer}
        execute-keys %opt{grep_current_line}g
    }}
}
define-command my-grep-previous-match \
    -docstring 'Jump to the previous match in a grep-like buffer' %{
    evaluate-commands -try-client %opt{jumpclient} %{
        buffer %opt{current_grep_buffer}
        execute-keys "%opt{grep_current_line}g<a-h> <a-/>^[^:]+:\d+:<ret>"
        grep-jump
        info "[↓↑=%opt{current_grep_buffer}]"
    }
    try %{ evaluate-commands -client %opt{toolsclient} %{
        buffer %opt{current_grep_buffer}
        execute-keys %opt{grep_current_line}g
    }}
}

map global normal <up>   ': my-grep-previous-match<ret>'
map global normal <down> ': my-grep-next-match<ret>'

hook global WinSetOption filetype=(grep|make|cargo) %{
    map window normal = ': toolsclient '
    map window normal <tab>   ': try %{ focus %opt{jumpclient} }<ret>'
    map window normal <a-tab> ': jumpclient '
    map window normal <left>  ': relate-previous<ret>'
    map window normal <right> ': relate-next<ret>'
}

define-command toolsclient -params ..1 \
-shell-script-candidates %{echo "$kak_client_list" | tr ' ' '\n' | rg -v "${kak_client:-^$}"} \
-docstring 'toolsclient [<jumpclient>]: set current client as toolsclient' \
%{
    rename-client tools
    set-option global last_focus_in %val{client}
    set-option global toolsclient %val{client}
    jumpclient %arg{1}
}

define-command jumpclient -params ..1 \
-shell-script-candidates %{echo "$kak_client_list" | tr ' ' '\n' | rg -v "${kak_opt_toolsclient:-^$}"} \
-docstring 'jumpclient [<client>]: set buffer jumpclient for toolsclient' \
%{
    evaluate-commands -try-client %opt{toolsclient} %sh{
        client="$1"
        if [ -z "$client" ]; then
            client="$kak_client"
        fi
        printf 'set-option buffer jumpclient "%s"\n' "$client"
    }
}

hook global ClientClose .* %{
    evaluate-commands %sh{
        if [ "$kak_hook_param" = "$kak_opt_toolsclient" ]; then
            echo "set-option global toolsclient ''"
        fi
    }
}

define-command tools-focus -params 1 \
-shell-script-candidates %{echo "$kak_buflist" | tr ' ' '\n' | rg '^\*\w+\*' | rg -v debug} \
%{
    evaluate-commands -try-client %opt{toolsclient} %{
        buffer %arg{1}
        focus
    }
}

define-command persistent-grep %{
    # exec <a-i>w
    evaluate-commands -save-regs 'g' %{
        set-register g %val{selection}
        grep %val{reg_g}
        try %{delete-buffer! "*grep*:%val{reg_g}"}
        rename-buffer "*grep*:%val{reg_g}"
        mark-pattern set %val{reg_g}
        set-option global current_grep_buffer %val{bufname}
    }
}
