## base16-kakoune (https://github.com/leira/base16-kakoune)
## mod by vbauerster

evaluate-commands %sh{

    base00="rgb:FDFDFD"
    base01="rgb:e0e0e0"
    base02="rgb:d6d6d6"
    base03="rgb:8e908c"
    base04="rgb:969896"
    base05="rgb:4d4d4c"
    base06="rgb:282a2e"
    base07="rgb:1d1f21"
    base08="rgb:c82829"
    base09="rgb:f5871f"
    base0A="rgb:eab700"
    base0B="rgb:718c00"
    base0C="rgb:3e999f"
    base0D="rgb:4271ae"
    base0E="rgb:8959a8"
    base0F="rgb:a3685a"

    white="rgb:FFFFFF"
    gold="rgb:ffd700"
    # match_bg="rgb:fdf6e2"

    # High contrast alternative text
    vibrant_orange="rgb:EE7733"
    vibrant_blue="rgb:0077BB"
    vibrant_cyan="rgb:33BBEE"
    vibrant_red="rgb:CC3311"
    vibrant_teal="rgb:009988"

    # Darker text with no red
    # muted_rose="rgb:CC6677"
    muted_wine="rgb:882255"

    # Pale background colors, black foreground
    pale_cyan="rgb:CCEEFF"
    pale_yellow="rgb:EEEEBB"

    cat <<- EOF

    # For Code
    face global keyword   ${base0E}+b
    face global attribute ${base08}
    face global type      ${base0F}
    face global string    ${base0B}
    face global value     ${base09}
    face global meta      ${vibrant_teal}
    face global builtin   ${base05}+b
    face global module    ${muted_wine}
    face global comment   ${base04}+i
    face global documentation comment
    face global function  ${vibrant_blue}
    face global operator  ${base0F}
    face global variable  default

    # For markup
    face global title  ${base0D}+b
    face global header ${base0D}
    face global mono   ${base0B}
    face global block  ${base09}
    face global link   ${base0C}+u
    face global list   Default
    face global bullet +b

    face global Default            ${base05},${base00}

    face global PrimarySelection   ${base06},${pale_cyan}
    face global PrimaryCursor      ${white},${vibrant_orange}+fg
    face global PrimaryCursorEol   ${white},${vibrant_red}+fg
    face global SecondarySelection ${pale_cyan},${base04}
    face global SecondaryCursor    ${base00},${vibrant_cyan}+fg
    face global SecondaryCursorEol ${base00},${vibrant_blue}+fg

    face global MatchingChar       ${base07},${base01}
    face global Search             ${base05},${gold}+i
    face global CurrentWord        ${base08},${base01}
    face global Whitespace         ${base01}+f
    face global WrapMarker         ${base05}+f
    face global BufferPadding      ${base03},${base00}
    face global LineNumbers        ${base02},${base00}
    face global LineNumberCursor   ${base0A},${base00}
    face global LineNumbersWrapped ${base00},${base00}
    face global MenuForeground     ${base00},${base0F}
    face global MenuBackground     ${base00},${base04}
    face global MenuInfo           ${pale_yellow}+i
    face global Information        ${base05},${pale_yellow}
    face global Error              ${white},${base08}
    face global StatusLine         ${base00},${base0C}
    face global StatusLineMode     ${base00},${base0B}
    face global StatusLineInfo     ${base01}
    face global StatusLineValue    ${base00}
    face global StatusCursor       ${base00},${base05}
    face global Prompt             ${base00},${base0D}

EOF
}

hook global FocusIn .* %{
    # echo -debug FocusIn %val{hook_param}
    unset-face window StatusLine
    evaluate-commands %sh{
        base00="rgb:FDFDFD"
        base03="rgb:8e908c"
        for client in $kak_client_list; do
            if [ "$client" != "$kak_hook_param" ]; then
                # printf "echo -debug dim sl for %s\n" "$client"
                # printf "face buffer StatusLine %s,%s\n" "rgb:FDFDFD" "rgb:969896"
                printf 'eval -no-hooks -client %s %%{face window StatusLine %s,%s}\n' \
                "$client" "$base00" "$base03"
            fi
        done
    }
}

hook global WinDisplay .* %{
    # echo -debug WinCreate %val{hook_param}
    unset-face window StatusLine
    # evaluate-commands %sh{
    #     for win in '*debug*' '*symbols*'; do
    #         if [ "$win" = "$kak_hook_param" ]; then
    #             exit 0
    #         fi
    #     done
    #     printf "echo -debug WinDisplay %s\n" "$kak_hook_param"
    #     echo "unset-face window StatusLine"
    # }
}
