# Go
# ‾‾‾‾
hook global WinSetOption filetype=go %{
    smarttab
    # set-option window matching_pairs '(' ')' '{' '}' '[' ']'
    # set-option window indentwidth 0 # 0 means real tab
    set-option -add window grepcmd ' -tgo -g=!vendor'
    set-option window formatcmd 'goimports'
    set-option window lintcmd 'gometalinter .'
    set-option window makecmd 'go build .'
    # set window lintcmd '(gometalinter | grep -v "::\w")  <'

    # https://discuss.kakoune.com/t/what-is-the-right-way-to-do-format-on-write/922
    # hook buffer BufWritePre .* %{ format }

    # define-command -docstring "Dim all error checks" go-err-chk-dim %{
    #     add-highlighter window/GoErrCheck regex 'if err != nil .*?\{.*?\}' 0:comment
    # }
    # define-command -docstring "Undim all error checks on" go-err-chk-on %{
    #     remove-highlighter window/GoErrCheck
    # }

    # kak-tree
    map window syntax-tree t ':tree-select-children type_declaration<ret>'            -docstring 'type_declaration'
    map window syntax-tree m ':tree-select-children method_declaration<ret>'          -docstring 'method_declaration'
    map window syntax-tree f ':tree-select-children function_declaration<ret>'        -docstring 'function_declaration'
    map window syntax-tree l ':tree-select-children func_literal<ret>'                -docstring 'func_literal'
    map window syntax-tree g ':tree-select-children go_statement<ret>'                -docstring 'go_statement'
    map window syntax-tree i ':tree-select-children if_statement<ret>'                -docstring 'if_statement'
    map window syntax-tree o ':tree-select-children for_statement<ret>'               -docstring 'for_statement'
    map window syntax-tree u ':tree-select-children parameter_list<ret>'              -docstring 'parameter_list'
    map window syntax-tree r ':tree-select-children return_statement<ret>'            -docstring 'return_statement'
    map window syntax-tree s ':tree-select-children expression_switch_statement<ret>' -docstring 'switch statement'
    map window syntax-tree c ':tree-select-children expression_case<ret>'             -docstring 'expression_case'
    map window syntax-tree C ':tree-select-children communication_case<ret>'          -docstring 'communication_case'
}

# Rust
# ‾‾‾‾
hook global WinSetOption filetype=rust %[
    expandtab
    # set-option window softtabstop 4
    set-option window formatcmd 'rustfmt'
    set-option -add window grepcmd ' -trust -g=!target'
    set-register @ 'A;<esc>h'
    map window normal ^ 'A,<esc>h'
    map window insert <s-ret> '<esc>: exec -with-hooks %{A;<lt>ret>}<ret>'
    # <a-ret> used by snippets
    # map window insert <a-ret> '<esc>: exec -with-hooks %{A,<lt>ret>}<ret>'

    map window lang-mode c ': enter-user-mode cargo<ret>' -docstring 'cargo'
    # kak-tree
    map window syntax-tree f -docstring 'function_item' \
    ':tree-select-children function_item<ret>'
    map window syntax-tree s -docstring 'scoped_identifier' \
    ':tree-select-children scoped_identifier<ret>'
    map window syntax-tree / -docstring 'line_comment' \
    ':tree-select-children line_comment<ret>'

    hook -- window InsertChar - %{
        try %{
            # execute-keys -draft <a-m>Gi <a-k>\bfn\s+\w+\b<ret>
            # execute-keys -draft <a-m>_ <a-k>\(.*?\)<ret>
            execute-keys -draft 2h2H <a-k>\Q)<space><minus>\E<ret>
            execute-keys <backspace><gt><space>
        }
        try %{
            execute-keys -draft hH <a-k>\Q&<minus>\E<ret>
            execute-keys <backspace>'
        }
    }
    # hook window InsertChar \$ %{ try %{
    #     execute-keys -draft gll<a-a>B<a-S><a-space>x <a-k>\bmatch\b<ret>
    #     execute-keys <backspace>=>
    # }}

    hook window InsertChar \? %[ try %[
        execute-keys -draft hH <a-k>\Q{?\E<ret>
        execute-keys <left>:<right>
    ]]
    hook window InsertChar '#' %[ try %[
        execute-keys -draft hH <a-k>\Q{#\E<ret>
        execute-keys <left>:<right>?
    ]]
    hook window InsertChar \| %{ try %{
        execute-keys -draft h2H <a-k>\Q|||\E<ret>
        execute-keys <backspace><left>
    }}
    hook window InsertChar > %{ try %{
        execute-keys -draft h2H <a-k>\Q<lt>>>\E<ret>
        execute-keys <backspace><left>
    }}
]

# Makefile
# ‾‾‾‾‾‾‾‾
# hook global BufCreate .*\.mk$ %{
#     set-option buffer filetype makefile
# }

hook global WinSetOption filetype=sql %{
    map buffer lang-mode o %{: grep ^INSERT|^UPDATE|^DELETE|^CREATE|^DROP' %val{bufname} -H -i<ret>} -docstring "Show outline"
}

# Highlight any files whose names start with "zsh" as sh
hook global BufCreate (.*/)?\.?zsh.* %{
    set-option buffer filetype sh
}

# Highlight files ending in .conf as ini
# (Will probably be close enough)
hook global BufCreate .*\.conf %{
    set-option buffer filetype ini
}

hook global WinSetOption filetype=json %{
    expandtab
    set window indentwidth 2
    set window formatcmd 'prettier --stdin --parser json'
    hook buffer BufWritePre .* %{format}
}
hook global WinSetOption filetype=markdown %{
    expandtab
    set window formatcmd 'prettier --stdin --parser markdown'
    hook buffer BufWritePre .* %{format}
    # map window user o %{: grep HACK|TODO|FIXME|XXX|NOTE|^# %val{bufname} -H<ret>} -docstring "Show outline"
}
hook global WinSetOption filetype=html %{
    set window indentwidth 2
    set window formatcmd 'prettier --stdin --parser html'
    hook buffer BufWritePre .* %{format}
}
hook global WinSetOption filetype=yaml %{
    expandtab
    set-option buffer tabstop 2
    set-option buffer indentwidth 2
    set-option buffer softtabstop 2
}

hook global WinSetOption filetype=git-status %{
    map window normal c ': git commit --verbose '
    map window normal l ': git log --oneline --graph<ret>'
    map window normal d ': -- %val{selections}<a-!><home> git diff '
    map window normal D ': -- %val{selections}<a-!><home> git diff --cached '
    map window normal a ': -- %val{selections}<a-!><home> git add '
    map window normal A ': -- %val{selections}<a-!><home> repl git add -p '
    map window normal r ': -- %val{selections}<a-!><home> git reset '
    map window normal R ': -- %val{selections}<a-!><home> repl git reset -p '
    map window normal o ': -- %val{selections}<a-!><home> git checkout '
}
hook global WinSetOption filetype=git-log %{
    map window normal d     ': %val{selections}<a-!><home> git diff '
    map window normal <ret> ': %val{selections}<a-!><home> git show '
    map window normal r     ': %val{selections}<a-!><home> git reset '
    map window normal R     ': %val{selections}<a-!><home> repl git reset -p '
    map window normal o     ': %val{selections}<a-!><home> git checkout '
}
