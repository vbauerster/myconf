# buffer-next but related, as a linked list.
# Usage: Relate buffers together and spam ] until reaching the desired buffer (assuming you mapped ] to relate-next).
provide-module relate %{
  # Internal variables
  declare-option -hidden -docstring 'Next related buffer' str relate_next_buffer
  declare-option -hidden -docstring 'Previous related buffer' str relate_previous_buffer

  # Commands
  # Relate
  define-command relate -params 1 -buffer-completion -docstring 'Relate with the given buffer' %{
    set-option buffer relate_next_buffer %arg{1}
    evaluate-commands -buffer %arg{1} set-option buffer relate_previous_buffer %val{bufname}
  }

  # Relate back
  define-command relate-back -params 1 -buffer-completion -docstring 'Relate back with the given buffer' %{
    relate %arg{1}
    evaluate-commands -buffer %arg{1} relate %val{bufname}
  }

  # Next and previous related buffers
  define-command relate-next -docstring 'Move to the next related buffer' %{
    buffer %opt{relate_next_buffer}
  }

  define-command relate-previous -docstring 'Move to the previous related buffer' %{
    buffer %opt{relate_previous_buffer}
  }
}
