# search-highlighting.kak, simplified
define-command search-highlighting-enable %{
  hook window -group search-highlighting NormalKey [/?nN]|<a-[/?nN]> %{ try %{
    addhl window/SearchHighlighting dynregex '%reg{/}' 0:Search
  }}
  hook window -group search-highlighting NormalKey <esc> %{ rmhl window/SearchHighlighting }
}
define-command search-highlighting-disable %{
  rmhl window/SearchHighlighting
  rmhooks window search-highlighting
}

# https://discuss.kakoune.com/t/display-all-faces-in-the-current-color-scheme/1178
declare-option -hidden range-specs face_colors
define-command color-faces %{
    buffer *debug*
    debug faces
    try %{ remove-highlighter buffer/face-colors }
    set-option buffer face_colors %val{timestamp}

    evaluate-commands -draft %{
        execute-keys '%' s^Faces:\n(<space>\*<space>[^\n]*\n)*<ret>
        execute-keys s ^<space>\*<space><ret>
        execute-keys lt:

        evaluate-commands -itersel %{
            eval %sh{
                printf "set-option -add buffer face_colors %s|%s\n" \
                "$kak_selection_desc" "$kak_selection"
            }
        }
    }

    add-highlighter buffer/face-colors ranges face_colors
}

# https://discuss.kakoune.com/t/type-0-to-show-relative-line-numbers-for-the-next-command/760/3
define-command switch-numbers -params .. %{
    try %{ gutter-off }
    gutter-on %arg{@}
}

define-command -hidden shift-to-numbers %{
    map window normal <[> 7
    map window normal <{> 5
    map window normal <}> 3
    map window normal <(> 1
    map window normal <=> 9
    map window normal <*> 0
    map window normal <)> 2
    map window normal <+> 4
    map window normal <]> 6
    map window normal <!> 8
    map window normal <down> 'j: shift-to-parens<ret>'
    map window normal <up> 'k: shift-to-parens<ret>'
    map window normal <esc> ': shift-to-parens<ret>'
}

define-command -hidden shift-to-parens %{
    unmap window normal <[>
    unmap window normal <{>
    unmap window normal <}>
    unmap window normal <(>
    unmap window normal <=>
    unmap window normal <*>
    unmap window normal <)>
    unmap window normal <+>
    unmap window normal <]>
    unmap window normal <!>
    unmap window normal <down>
    unmap window normal <up>
    unmap window normal <esc>
    switch-numbers
}

map global normal 0 ': zero "eval switch-numbers -relative;shift-to-numbers"<ret>'

declare-option -hidden bool lsp_enabled
define-command gutter-on -params .. %{
    add-highlighter window/numbers number-lines -hlcursor -separator ' ' %arg{@}
    add-highlighter window/git-diff flag-lines Default git_diff_flags
    evaluate-commands %sh{
        if [ "$kak_opt_lsp_enabled" = "true" ]; then
            printf "lsp-diagnostic-lines-enable window\n"
        fi
    }
}
define-command gutter-off %{
    try %{
        lsp-diagnostic-lines-enable window
        lsp-diagnostic-lines-disable window
        set-option window lsp_enabled false
    } catch %{
        lsp-diagnostic-lines-disable window
        set-option window lsp_enabled true
    }
    remove-highlighter window/git-diff
    remove-highlighter window/numbers
}
define-command numbers-off %{
    remove-highlighter window/numbers
}
define-command -hidden word-wrap-on %{
    add-highlighter window/wrap wrap -word -indent -marker ↪
}
define-command -hidden word-wrap-off %{
    remove-highlighter window/wrap
}
define-command word-wrap-toggle %{
    try %{
        word-wrap-on
        echo -markup "{Information}word wrap on"
    } catch %{
        word-wrap-off
        echo -markup "{Information}word wrap off"
    }
}
define-command -hidden whitespaces-show %{
    add-highlighter window/whitespaces show-whitespaces -tab '›' -tabpad '⋅' -spc ' ' -nbsp '⍽'
}
define-command -hidden whitespaces-hide %{
    remove-highlighter window/whitespaces
}
define-command whitespaces-toggle %{
    try %{
        whitespaces-show
    } catch %{
        whitespaces-hide
    }
}

add-highlighter global/visible-words regex \b(?:FIXME|TODO|NOTE)\b 0:+ub

hook global WinCreate .* %{
    # https://github.com/mawww/kakoune/issues/4052
    whitespaces-show
    gutter-on
    word-wrap-on
    add-highlighter window/matching show-matching
    search-highlighting-enable
    # enable tab complete in insert mode
    tab-completion-enable
}

hook global WinSetOption filetype=(grep|make|git-commit|cargo) %{
    gutter-off
    word-wrap-off
    whitespaces-hide
}

# Diff
# ‾‾‾‾
# https://github.com/eraserhd/dotfiles/blob/ad8434deef29e79477530c36317a7f0580aae8de/kakoune/kakrc#L512
hook -group diff-text-objects global WinSetOption filetype=diff %{
    word-wrap-off
    whitespaces-hide
    map window -docstring 'diff file' object f '<a-;> diff-select-file<ret>'
    map window -docstring 'diff hunk' object h '<a-;> diff-select-hunk<ret>'
    hook -once -always window WinSetOption filetype=.* %{
        unmap window object f
        unmap window object h
    }
}

add-highlighter shared/diff/comment regex '^#[^\n]*' 0:comment
add-highlighter shared/diff/feedback regex '^>[^\n]*' 0:title

# StatusLine face toggle
# ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
declare-option -hidden str last_focus_in
declare-option -hidden str focus_out_face "rgb:FDFDFD,rgb:8e908c"
hook global -group status-line-face-toggle FocusIn .* %{
    # echo -debug FocusIn %val{hook_param}
    set-option global last_focus_in %val{hook_param}
    unset-face window StatusLine
    evaluate-commands %sh{
        for client in $kak_client_list; do
            if [ "$client" != "$kak_hook_param" ]; then
                printf "eval -no-hooks -client %s 'set-face window StatusLine %s'\n" \
                "$client" "$kak_opt_focus_out_face"
            fi
        done
    }
}

hook global -group status-line-face-toggle WinDisplay .* %{
    evaluate-commands %sh{
        if [ "$kak_client" = "$kak_opt_last_focus_in" ]; then
            echo "unset-face window StatusLine"
        elif [ -n "$kak_opt_last_focus_in" ]; then
            printf "set-face window StatusLine %s\n" "$kak_opt_focus_out_face"
        fi
    }
}
