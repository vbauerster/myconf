define-command -docstring "evaluate-buffer: evaluate current buffer contents as kakscript" \
evaluate-buffer %{
    execute-keys -draft '%:<space><c-r>.<ret>'
}

define-command -docstring "evaluate-selection: evaluate current sellection contents as kakscript" \
evaluate-selection %{
    execute-keys -itersel -draft ':<space><c-r>.<ret>'
}

# Tab completion.
define-command tab-completion-enable %{
  hook -group tab-completion window InsertCompletionShow .* %{ try %{
    # execute-keys -draft 'h<a-K>\h<ret>'
    map window insert <tab> <c-n>
    map window insert <s-tab> <c-p>
    map window insert <c-space> <c-o>
  }}
  hook -group tab-completion window InsertCompletionHide .* %{
    unmap window insert <tab> <c-n>
    unmap window insert <s-tab> <c-p>
    unmap window insert <c-space> <c-o>
  }
}
define-command tab-completion-disable %{ remove-hooks global tab-completion }

define-command pairwise-enable %~
    auto-pairs-disable
    hook -group pairwise global InsertChar \) %{ try %{
        execute-keys -draft h2H <a-k>\Q())\E<ret>
        execute-keys <backspace><left>
    }}
    hook -group pairwise global InsertChar \] %{ try %{
        execute-keys -draft h2H <a-k>\Q[]]\E<ret>
        execute-keys <backspace><left>
    }}
    hook -group pairwise global InsertChar \} %[ try %[
        execute-keys -draft h2H <a-k>\Q{}}\E<ret>
        execute-keys <backspace><left>
    ]]
~
define-command pairwise-disable %{
    remove-hooks global pairwise
    auto-pairs-enable
}

define-command trailing-whitespace-del %{
  try %{
    eval -draft %{
      exec '%s\h+$<ret><a-d>'
      eval -client %val{client} echo -markup -- \
        %sh{ echo "{Information}deleted trailing whitespace on $(echo "$kak_selections_desc" | wc -w) lines" }
    }
  } catch %{
    echo -markup "{Information}no trailing whitespace"
  }
}

# define-command enlarge-selection %{
#   exec '<a-:>L<a-;>H<a-:>'
# }
# define-command shrink-selection %{
#   exec '<a-:>H<a-;>L<a-:>'
# }
# define-command shift-selection-left %{
#   exec '<a-:>H<a-;>H<a-:>'
# }
# define-command shift-selection-right %{
#   exec '<a-:>L<a-;>L<a-:>'
# }

define-command slice-by-word %{
  exec s[A-Z][a-z]+|[A-Z]+|[a-z]+<ret>
}

# https://github.com/mawww/kakoune/wiki/Selections#how-to-convert-between-common-case-conventions
# foo_bar → fooBar
# foo-bar → fooBar
# foo bar → fooBar
define-command camelcase %{
  exec '`s[-_<space>]<ret>d~<a-i>w'
}

# fooBar → foo_bar
# foo-bar → foo_bar
# foo bar → foo_bar
define-command snakecase %{
  exec '<a-:><a-;>s-|[a-z][A-Z]<ret>\;a<space><esc>s[-\s]+<ret>c_<esc><a-i>w`'
}

# fooBar → foo-bar
# foo_bar → foo-bar
# foo bar → foo-bar
define-command kebabcase %{
  exec '<a-:><a-;>s_|[a-z][A-Z]<ret>\;a<space><esc>s[_\s]+<ret>c-<esc><a-i>w`'
}

# https://github.com/mawww/kakoune/wiki/Selections#how-to-make-x-select-lines-downward-and-x-select-lines-upward
define-command -hidden -params 1 extend-line-down %{
  exec "<a-:>%arg{1}X"
}
define-command -hidden -params 1 extend-line-up %{
  exec "<a-:><a-;>%arg{1}K<a-;>"
  try %{
    exec -draft ';<a-K>\n<ret>'
    exec X
  }
  exec '<a-;><a-X>'
}

# https://github.com/mawww/kakoune/wiki/Normal-mode-commands#suggestions
# Bind things that don't take numeric arguments to the keys 0/<backspace>.
# Usage: map global normal 0 ': zero "exec gh"<ret>'
#        map global normal <backspace> ': backspace "exec h"<ret>'
def zero      -hidden -params 1 %{ eval %sh{[ "$kak_count" -eq 0 ] && echo "$1" || echo "exec '${kak_count}0'"} }
def backspace -hidden -params 1 %{ eval %sh{[ "$kak_count" -eq 0 ] && echo "$1" || echo "exec '${kak_count%?}'"} }

# https://github.com/alyssais/dotfiles/blob/master/.config/kak/kakrc#L30-L38
define-command -docstring "import from the system clipboard" clipboard-import %{
  set-register dquote %sh{pbpaste}
  echo -markup "{Information}imported system clipboard to "" register"
}
define-command -docstring "export to the system clipboard" clipboard-export %{
  nop %sh{ printf "%s" "$kak_main_reg_dquote" | pbcopy }
  echo -markup "{Information}exported "" register to system clipboard"
}
define-command -docstring 'choose from tmux buffer into " reg' tmux-choose-buffer %{
    evaluate-commands %sh{
        output=$(mktemp -d)
        # https://hackaday.com/2017/07/21/linux-fu-better-bash-scripting
        trap 'rm -Rf "$output"; exit' INT TERM EXIT
        fifo="$output/buffer.tmp"
        mkfifo "$fifo"
        tmux choose-buffer "save-buffer -b '%%' ""$fifo""" && pbcopy < "$fifo"
        echo 'clipboard-import'
    }
}

# Sort of a replacement for gq.
# https://github.com/mawww/kakoune/wiki/Wrapping
# https://discuss.kakoune.com/t/smarter-replacement-for-coreutils-fmt/1152
# define-command format-text %{ exec '|fmt %opt{autowrap_column}<a-!><ret>' }
define-command format-text %{
  exec '|par "rTbqR" "B=.,?_A_a Q=_s>|" -w%opt{autowrap_column}<a-!><ret>'
}
define-command format-comment %{ exec '<a-s>ght/F<space>dx<a-_>|fmt<a-!><ret><a-s>Px<a-_>' }

# Git extras.
# https://github.com/shachaf/kak/blob/c2b4a7423f742858f713f7cfe2511b4f9414c37e/kakrc#L381
define-command git-show-blamed-commit %{
  git show %sh{git blame -L "$kak_cursor_line,$kak_cursor_line" "$kak_buffile" | awk '{print $1}'}
}
define-command git-log-lines %{
  git log -L %sh{
    anchor="${kak_selection_desc%,*}"
    anchor_line="${anchor%.*}"
    echo "$anchor_line,$kak_cursor_line:$kak_buffile"
  }
}
define-command git-toggle-blame %{
  try %{
    addhl window/git-blame group
    rmhl window/git-blame
    git blame
  } catch %{
    git hide-blame
  }
}

# https://github.com/shachaf/kak/blob/c2b4a7423f742858f713f7cfe2511b4f9414c37e/kakrc#L355
define-command -docstring %{switch to the other client's buffer} \
  other-client-buffer \
  %{ eval %sh{
  if [ "$(echo "$kak_client_list" | wc -w)" -ne 2 ]; then
    echo "fail 'only works with two clients'"
    exit
  fi
  set -- $kak_client_list
  other_client="$1"
  [ "$other_client" = "$kak_client" ] && other_client="$2"
  echo "eval -client '$other_client' 'eval -client ''$kak_client'' \"buffer ''%val{bufname}''\"'"
}}

# https://github.com/mawww/kakoune/wiki/Selections#how-to-select-the-smallest-single-selection-containing-every-selection
# https://github.com/shachaf/kak/blob/c2b4a7423f742858f713f7cfe2511b4f9414c37e/kakrc#L302
define-command selection-hull \
  -docstring 'The smallest single selection containing every selection.' \
  %{
  eval -save-regs 'ab' %{
    exec '"aZ' '<space>"bZ'
    try %{ exec '"az<a-space>' }
    exec -itersel '"b<a-Z>u'
    exec '"bz'
    echo
  }
}

define-command -docstring 'flygrep: run grep on every key' \
flygrep %{
    edit -scratch *grep*
    prompt "flygrep: " -on-change %{
        flygrep-call-grep %val{text}
    } nop
}
define-command -hidden flygrep-call-grep -params 1 %{ evaluate-commands %sh{
    [ -z "${1##*&*}" ] && text=$(printf "%s\n" "$1" | sed "s/&/&&/g") || text="$1"
    [ -z "${1##*@*}" ] && text=$(printf "%s\n" "$text" | sed "s/@/@@/g") || text="$text"
    if [ ${#1} -gt 2 ]; then
        printf "%s\n" "info"
        printf "%s\n" "evaluate-commands %&grep %@$text@&"
    else
        printf "%s\n" "info -title flygrep %{$((3-${#1})) more chars}"
    fi
}}

# https://github.com/mawww/kakoune/issues/1106
# https://discuss.kakoune.com/t/repeating-a-character-n-times-in-insert-mode/670
define-command -params 1 count-insert %{
    execute-keys -with-hooks \;i.<esc>hd %arg{1} Ph %arg{1} HLs.<ret>c
}

# Misc
# ‾‾‾‾

define-command read-only %{
  edit -readonly %val{bufname}
}

# https://discuss.kakoune.com/t/split/1079
declare-option -hidden str split_bufname
define-command -docstring "open the current buffer in a new client" split %{
    set-option global split_bufname %val{bufname}

    new 'edit %opt{split_bufname};' "select %val{selections_desc}"
    # NOTE:
    # %opt{split_bufname} must be single-quoted so it will be expanded
    # by the new client, ensuring it will treat it as a single argument
    # even if it has whitespace in it.
    # %val{selections_desc} must be double-quoted so it will be expanded
    # by this client into multiple arguments. The select command expects
    # each selection description to be a separate argument.
}

# https://github.com/robertmeta/kakfiles/blob/1659f1a9358a2728ab5419427634ded7d79ea976/kakrc#L189
define-command github-url \
    -docstring "github-url: copy the canonical GitHub URL to the system clipboard" \
    %{ evaluate-commands %sh{
        # use the remote configured for fetching
        fetch_remote=$(git config --get "branch.$(git symbolic-ref --short HEAD).remote" || printf origin)
        base_url=$(git remote get-url "$fetch_remote" | sed -e "s|^git@github.com:|https://github.com/|")
        # assume the master branch; this is what I want 95% of the time
        master_commit=$(git ls-remote "$fetch_remote" master | awk '{ print $1 }')
        relative_path=$(git ls-files --full-name "$kak_bufname")
        selection_start="${kak_selection_desc%,*}"
        selection_end="${kak_selection_desc##*,}"

        if [ "$selection_start" == "$selection_end" ]; then
            github_url=$(printf "%s/blob/%s/%s" "${base_url%.git}" "$master_commit" "$relative_path")
        else
            start_line="${selection_start%\.*}"
            end_line="${selection_end%\.*}"

            # highlight the currently selected line(s)
            if [ "$start_line" == "$end_line" ]; then
                github_url=$(printf "%s/blob/%s/%s#L%s" "${base_url%.git}" "$master_commit" "$relative_path" "${start_line}")
            else
                github_url=$(printf "%s/blob/%s/%s#L%s-L%s" "${base_url%.git}" "$master_commit" "$relative_path" "${start_line}" "${end_line}")
            fi
        fi
        # printf "execute-keys -draft '!printf %s $github_url | $kak_opt_system_clipboard_copy<ret>'\n"
        printf "set-register dquote '%s'\n" "$github_url"
        printf "echo -markup '{Information}%s'\n" "$github_url"
    }
}
# def mkdir %{ nop %sh{ mkdir -p $(dirname $kak_buffile) } } -docstring "Creates the directory up to this file"
# def delete-buffers-matching -params 1 %{ evaluate-commands -buffer * %{ evaluate-commands %sh{ case "$kak_buffile" in $1) echo "delete-buffer" esac } } }

define-command git-ls-files -docstring "list files with 'git ls-files'" \
-params 1 -shell-script-candidates 'git ls-files' %{ edit %arg{1} }

define-command rg-ls-files -docstring "list files with 'rg --files'" \
-params 1 -shell-script-candidates 'rg --files' %{ edit %arg{1} }

# define-command fd-edit-all -params 1 -shell-script-candidates 'rg -uu --files' %{ edit %arg{1} }

define-command show-char-info \
  -docstring 'show information about character under cursor' \
  %{
  # This is easy, but it's not a great way of getting Unicode data (doesn't have
  # names for control characters, isn't up to date).
  echo %sh{
  python3 <<-'EOF'
import os, unicodedata
char = chr(int(os.environ['kak_cursor_char_value']))
try:
  info = '{} [{}]'.format(unicodedata.name(char), unicodedata.category(char))
except ValueError as e:
  info = str(e)
print('U+{:x}: {}'.format(ord(char), info))
EOF
  }
}

# https://discuss.kakoune.com/t/sublime-text-style-multiple-cursor-select-add-mapping/150
define-command -hidden -docstring \
"select a word under cursor, or add cursor on next occurrence of current selection" \
select-or-add-cursor -params 1 %{
    try %{
        execute-keys "<a-k>\A.\z<ret>"
        exec <a-i>w
    } catch %{
        evaluate-commands %sh{
            case "$1" in
                (up) dir_key="<a-N>";;
                (down) dir_key="N";;
            esac
            printf "execute-keys *%s\n" "$dir_key"
        }
    } catch nop
}

# https://github.com/shachaf/kak/blob/c2b4a7423f742858f713f7cfe2511b4f9414c37e/kakrc#L241
define-command switch-to-modified-buffer %{
  eval -save-regs a %{
    reg a ''
    try %{
      eval -buffer * %{
        eval %sh{[ "$kak_modified" = true ] && echo "reg a %{$kak_bufname}; fail"}
      }
    }
    eval %sh{[ -z "$kak_main_reg_a" ] && echo "fail 'No modified buffers!'"}
    buffer %reg{a}
  }
}

define-command -docstring 'expand and edit dquote string' \
expand-edit %{
    execute-keys -with-hooks ': explore-files <c-r>.<a-!><ret>'
}

# https://discuss.kakoune.com/t/howdoi-instant-answers-from-the-kak-command-line/777/4
declare-option -hidden str cheat_filetype
define-command cheat -params 1.. \
-shell-script-candidates %{echo "$kak_opt_cheat_list_candidates"} \
%{
    try %{
        delete-buffer! *cheat*
    } catch %{
        set-option global cheat_filetype %opt{filetype}
    }
    edit -scratch *cheat*
    execute-keys "!curl -s cheat.sh/%opt{cheat_filetype}/%sh{echo $@ | tr ' ' '+'}?style=lovelace<ret>"
    ansi-render
}

declare-option -hidden str cheat_list_candidates
define-command cheat-populate %{
    evaluate-commands %sh{
        ft=${kak_opt_cheat_filetype:-${kak_opt_filetype}}
        list=$(curl -s cheat.sh/${ft}/:list | rg -Fv :list)
        printf 'set-option global cheat_list_candidates "%s"\n' "$list"
        printf 'echo list candidates were set for filetype=%s' "$ft"
    }
}

# # https://github.com/shachaf/kak/blob/c2b4a7423f742858f713f7cfe2511b4f9414c37e/kakrc#L347
# def man-selection-with-count %{
#   man %sh{
#     page="$kak_selection"
#     [ "$kak_count" != 0 ] && page="${page}(${kak_count})"
#     echo "$page"
#   }
# }
# def align-cursors-left \
#   -docstring 'set all cursor (and anchor) columns to the column of the leftmost cursor' \
#   %{ eval %sh{
#   col=$(echo "$kak_selections_desc" | tr ' ' '\n' | sed 's/^[0-9]\+\.[0-9]\+,[0-9]\+\.//' | sort -n | head -n1)
#   sels=$(echo "$kak_selections_desc" | sed "s/\.[0-9]\+/.$col/g")
#   echo "select $sels"
# }}

# def selection-length %{echo %sh{echo ${#kak_selection} }}
# define-command selection-length %{
#     eval %sh{ echo "echo ${#kak_selection}" }
# }

# define-command ide %{
#     rename-client main
#     set global jumpclient main
#     new rename-client tools
#     set global toolsclient tools
# }

