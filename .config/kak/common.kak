## General settings.
set-option -add global ui_options ncurses_status_on_top=yes
set-option -add global ui_options ncurses_assistant=off

set-option global autoreload yes
set-option global tabstop 4
set-option global indentwidth 4
# https://discuss.kakoune.com/t/how-to-page-move-while-keeping-cursor-centered/1241
set-option global scrolloff 5,1

# Grep
set-option global grepcmd 'rg --column'

colorscheme second-light

# Aliases
# ‾‾‾‾‾‾‾
alias global explore-files fzf-files
alias global explore-buffers fzf-buffers
alias global pwd print-working-directory
alias global bs buffer-switcher
alias global h search-doc

alias global cs colorscheme
alias global bd delete-buffer
alias global f rg-ls-files
alias global g git-ls-files
alias global ro read-only
alias global wqa write-all-quit
alias global wq write-quit
alias global wq! write-quit!
alias global u enter-user-mode

## Maps.
map global normal = ': enter-user-mode syntax-tree<ret>'
map global normal , ': enter-user-mode anchor<ret>'
map global normal <space> ,
map global normal <:>   ': select-or-add-cursor down<ret>'
map global normal <a-:> ': select-or-add-cursor up<ret>'
map global normal <&> <a-x> -docstring 'whole block'
map global normal <%> <a-X> -docstring 'inner block'
map global normal <c-i> <tab>
map global normal <@> q
map global normal <a-@> Q
map global normal <a-r> <">
map global normal <#>  ': comment-line<ret>'
map global normal <F1> ': doc keys<ret>'
map global normal <F2> ': w<ret>'
map global normal <c-]> ': enter-user-mode -lock vv-np<ret>'

map global normal <minus> %{:f }
map global normal <a-minus> %{:g }

map global normal <a-x> <a-i><a-w>
map global normal <a-q> x
map global normal X <c-s>%
map global normal q X
map global normal Q <a-semicolon> -docstring 'flip cursor and anchor'
map global normal <tab> <&>       -docstring 'align cusors'

map global normal <c-x> ': split<ret>'
map global normal D '<a-:><a-;>i<backspace><esc>'

# stop c and d from yanking
map global normal d <a-d>
map global normal <a-d> d
map global normal c <a-c>
map global normal <a-c> c

# https://discuss.kakoune.com/t/taking-back-control-of-hjkl-with-modifiers-keys/1077
map global normal <a-j> C     -docstring 'copy selection on next line'
map global normal <a-k> <a-C> -docstring 'copy selection on previous line'
map global normal <a-J> ': extend-line-down %val{count}<ret>'
map global normal <a-K> ': extend-line-up %val{count}<ret>'
map global normal C <a-j>
map global normal <a-C> <a-J>
map global normal Y <a-k>
map global normal <a-Y> <a-K>

## Plugs
map global normal <backspace> ': backspace "eval buffer-switcher"<ret>'
map global normal <a-down>    ': move-line-below<ret>'
map global normal <a-up>      ': move-line-above<ret>'
map global normal <c-t>       ': connect-terminal<ret>'
map global normal x      '<c-s>: expand<ret>' -docstring 'kakoune-expand'
map global normal <left>      ': buffer-previous;info-buffers<ret>' -docstring 'prev buffer'
map global normal <right>     ': buffer-next;info-buffers<ret>' -docstring 'next buffer'
map global normal <c-.>       ': inc-dec-modify-numbers + %val{count}<ret>'
map global normal <c-,>       ': inc-dec-modify-numbers - %val{count}<ret>'
# map global normal <a-I>       ': enter-user-mode split-object<ret>'

## User
# map global user <%> ': evaluate-buffer<ret>' -docstring 'source buffer'
map -docstring 'drop non main selections' global user <space> <space>
map -docstring 'drop main selection'      global user <semicolon> <a-space>
map -docstring 'jump to a tag definition' global user <tab> ': tag '
map -docstring 'prompt'                   global user <ret> <:>
map -docstring 'source selection'         global user . ': evaluate-selection<ret>'
map -docstring 'toggle mode'              global user , ': enter-user-mode toggle<ret>'
map -docstring 'grep keymap mode'         global user / ': enter-user-mode grep<ret>'
map -docstring 'focus…'                   global user o ': enter-user-mode focus<ret>'
map -docstring 'lang mode'                global user m ': enter-user-mode lang-mode<ret>'
map -docstring 'window keymap mode'       global user w ': enter-user-mode tmux-window<ret>'
map -docstring 'spell keymap mode'        global user s ': enter-user-mode spell<ret>'
map -docstring 'clipboard mode'           global user y ': enter-user-mode clipboard<ret>'
map -docstring 'echo mode'                global user e ': enter-user-mode echo-mode<ret>'
map -docstring 'git mode'                 global user g ': enter-user-mode git<ret>'
map -docstring 'buffers mode'             global user x ': enter-buffers-mode<ret>'
map -docstring 'user mode'                global user u ': u '
map -docstring 'user mode -lock'          global user <a-u> ': u -lock '
map -docstring 'force-quit'               global user Q ': q!<ret>'
map -docstring 'write-quit'               global user Z ': wq<ret>'
# https://discuss.kakoune.com/t/vim-like-mark-sort-of/1233
declare-option -hidden str my_mark_reg
map global user <"> -docstring 'sel to reg' \
%{: prompt 'Register<lt>=:' 'exec \" %val{text}Z;set buffer my_mark_reg %val{text}'<ret>}
map global user <'> -docstring 'sel from reg' \
%{: prompt '<lt>=Register:' -init %opt{my_mark_reg} 'exec \" %val{text}z'<ret>}

## View
map -docstring 'scroll ↑'     global view e jv
map -docstring 'scroll ↓'     global view y kv
map -docstring '4j (sticky)'  global view j <esc>4jv
map -docstring '4k (sticky)'  global view k <esc>4kv
map -docstring '<a-i>'        global view i <esc><a-i>
map -docstring '<a-a>'        global view a <esc><a-a>
map -docstring 'split object' global view o '<esc>: enter-user-mode split-object<ret>'
map -docstring 'surround'     global view u '<esc>: enter-user-mode surround<ret>'
map -docstring 'select window viewport'  global view w '<esc><c-s>gtGbGl'
map -docstring 'toggle mark (bounded)'   global view <'>   %{<esc>: mark-pattern toggle '\b%val{selection}<a-!>\b'<ret>}
map -docstring 'toggle mark (unbounded)' global view <a-'> %{<esc>: mark-pattern toggle '%val{selection}<a-!>'<ret>}
map -docstring 'clear all mark hl'       global view <">   %{<esc>: mark-clear<ret>}
# map -docstring 'Select all occurrences of the current selection set in the window viewport' global view P '<esc>*gtGbGls<ret>'

## Goto
map -docstring 'buffer bottom'        global goto 'q' 'j'
map -docstring 'buffer end'           global goto 'Q' 'e'
map -docstring 'line begin'           global goto 'a' 'h'
map -docstring 'line end'             global goto 'e' 'l'
map -docstring 'end amd m'            global goto 'u' 'l<esc>m'
map -docstring 'window top'           global goto 'k' 't'
map -docstring 'window center'        global goto 'x' 'c'
map -docstring 'window bottom'        global goto 'j' 'b'
map -docstring 'kakoune-cd'           global goto 'c' '<esc>: enter-user-mode cd<ret>'
map -docstring 'expand and edit'      global goto 'F' '<esc>: exec <lt>a-i><lt>a-w>;expand-edit<ret>'
map -docstring '↔ last buffer'        global goto 'o' 'a: info-buffers<ret>'
map -docstring 'goto (extend to)'     global goto '<space>' '<esc>G'
map global goto t <esc>
map global goto b <esc>
map global goto h <esc>
map global goto l <esc>
# map -docstring 'switch to [+] buffer' global goto '<plus>' '<esc>: switch-to-modified-buffer<ret>'

## Insert mode
# <c-o>    ; # silent: stop completion
# <c-x>    ; # complete here
# <c-v>    ; # raw insert, use vim binding
# map global insert '<a-g>' '<esc>:'
# map global insert '<a-[>' '<a-;>'
# map global insert '<a-tab>' '<esc>'
# map global insert <a-c> '<esc>: comment-line<ret>'
map global insert <a-C> '<esc>x_<a-c>'
map global insert <c-a> '<esc>I'
map global insert <c-e> '<end>'
map global insert <a-h> '<left>'
map global insert <a-l> '<right>'
map global insert <c-{> '<left>'
map global insert <c-}> '<right>'
map global insert <a-m> '<a-;>vv'
map global insert <c-i> '<esc>: prompt Count: %{count-insert %val{text}}<ret>'
map global insert <a-t> '<esc><c-s>b;<a-`><c-o>i'
map global insert <a-T> '<esc><c-s>b<a-`><c-o>i'
map global insert <a-u> '<c-o><c-x>l'
map global insert <a-minus> '<esc><c-s>b: snakecase<ret><c-o>i'
map global insert <a-O> '<esc><c-s>O'
map global insert <a-r> '<c-r>"'
map global insert <c-y> '<a-;>!pbpaste<ret>'
map global insert <a-backspace> <esc>b<a-d>i
map global insert <c-h> <backspace>
map global insert <c-d> <del>
map global insert <ret> '<a-;>: snippets-enter auto-pairs-insert-new-line<ret>'
map global insert <s-ret> '<esc>o'

## Prompt
map global prompt -docstring 'case insensitive search' <a-i> '<home>(?i)<end>'
map global prompt -docstring 'regex disabled search' <a-q> '<home>\Q<end>\E<left><left>'
map global prompt -docstring 'bounded search' <a-minus> '<home>\b<end>\b<left><left>'
map global prompt -docstring 'expand to the buffer directory' <a-.> '%sh(dirname "$kak_buffile")<a-!>'
map global prompt -docstring 'split object' <a-s> '<esc>: enter-user-mode split-object<ret>'
map global prompt <a-r> '<c-r>"'
map global prompt <a-w> <a-f>
map global prompt <a-h> <c-w>
map global prompt <c-w> <c-f>

## Object
unmap global object f
unmap global object v
map global object h 'c\s,\s<ret>' -docstring 'select between whitespace'
map global object x '<esc>: text-object-buffer<ret>' -docstring 'buffer'
map global object y '<esc>: text-object-vertical<ret>' -docstring 'vertical selection'

## Editorconfig
## ‾‾‾‾‾‾‾‾‾‾‾‾
# hook global BufOpenFile .* editorconfig-load
# hook global BufNewFile  .* editorconfig-load

# escape hatch
# https://github.com/mawww/kakoune/wiki/Avoid-the-escape-key
hook global InsertChar \. %{ try %{
    execute-keys -draft hH <a-k>\Q,.\E<ret> d
    execute-keys -with-hooks <esc>
}}
hook global InsertChar \+ %{
    try %{
        execute-keys -draft hH <a-k>\Q,+\E<ret>
        execute-keys -with-hooks <backspace><esc>h
    }
    try %{
        execute-keys -draft %{ hH <a-k>\Q;+\E<ret> }
        execute-keys -with-hooks <backspace><esc>h
    }
    try %{
        execute-keys -draft hH <a-k>\Q]+\E<ret> d
        execute-keys -with-hooks <esc>
    }
}

# hook global BufWritePre .* %{ nop %sh{ mkdir -p $(dirname "$kak_hook_param") }}

## Git
## ‾‾‾
hook global BufOpenFile .* %{
    evaluate-commands -draft %sh{
        # printf "echo -debug %s\n" "$kak_hook_param"
        cd $(dirname "$kak_buffile")
        if [ $(git rev-parse HEAD 2>/dev/null) ]; then
            for hook in WinCreate BufReload BufWritePost; do
                printf "hook buffer -group git-update-diff %s .* 'git update-diff'\n" "$hook"
            done
        fi
    }
}

