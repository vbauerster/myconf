declare-user-mode focus
map -docstring 'tmux-focus'          global focus * ': focus '
map -docstring 'other-client-buffer' global focus o ': other-client-buffer<ret>'
map -docstring 'jumpclient'          global focus j ': try %{ focus %opt{jumpclient} }<ret>'
map -docstring 'toolsclient'         global focus t ': try %{ focus %opt{toolsclient} }<ret>'
map -docstring 'focus current tool'  global focus u ': tools-focus %opt{current_grep_buffer}<ret>'
# map -docstring 'manual'              global focus m ': tools-focus '
# map -docstring 'current_grep_buffer' global focus u ': tools-focus %opt{current_grep_buffer}<ret>'

## Spell
# https://discuss.kakoune.com/t/useful-user-modes/730/4
declare-user-mode spell
map global spell r ': spell ru<ret>' -docstring 'RU'
map global spell e ': spell en<ret>' -docstring 'ENG'
map global spell f ': spell-next<ret>_: enter-user-mode spell<ret>' -docstring 'next'
map global spell u ': spell-replace<ret><ret> : enter-user-mode spell<ret>' -docstring 'lucky fix'
map global spell a ': spell-replace<ret>' -docstring 'manual fix'
map global spell c ': spell-clear<ret>' -docstring 'clear'

declare-user-mode grep
map global grep / %{: grep -H -- '' %val{bufname}<a-B><left><left>} -docstring 'current buf only'
map global grep e %{: grep -- ''<left>} -docstring 'grep'
map global grep m ': grepmenu ' -docstring 'grepmenu'
map global grep p ': persistent-grep<ret>' -docstring 'persist selection'
map global grep f ': find ' -docstring 'find'
map global grep a ': find-apply-changes<ret>' -docstring 'find-apply-changes'

# https://discuss.kakoune.com/t/rfr-best-way-to-add-a-toggle/580/2
declare-user-mode toggle
map -docstring 'switch case'      global toggle ,     '<a-`>'
map -docstring 'jumpclient'       global toggle .     ': jumpclient<ret>'
map -docstring 'jumpclient ...'   global toggle <a-.> ': jumpclient '
map -docstring 'word case'        global toggle =     ': enter-user-mode word-case<ret>'
map -docstring 'require module'   global toggle m     ': require-module '
map -docstring 'colorscheme'      global toggle c     ': enter-user-mode toggle-colors<ret>'
map -docstring 'buffer settings'  global toggle b     ': enter-user-mode toggle-buffer<ret>'
map -docstring 'search highlight' global toggle h     ': search-highlighting-'
map -docstring 'numbers'          global toggle n     ': enter-user-mode toggle-numbers<ret>'
map -docstring 'word wrap'        global toggle r     ': word-wrap-toggle<ret>'
map -docstring 'autowrap'         global toggle <a-r> ': autowrap-'
map -docstring 'whitespaces'      global toggle w     ': whitespaces-toggle<ret>'
map -docstring 'auto-pairs'       global toggle <(>   ': auto-pairs-'
map -docstring 'tabs to spaces'   global toggle <@> <@>
map -docstring 'spaces to tabs'   global toggle <a-@> <a-@>
# map -docstring 'increment number' global toggle <plus> ': inc-dec-modify-numbers + %val{count}<ret>'
# map -docstring 'decrement number' global toggle <minus> ': inc-dec-modify-numbers - %val{count}<ret>'

declare-user-mode toggle-buffer
map -docstring 'set filetype'            global toggle-buffer 'b'       ': set buffer filetype '
map -docstring 'expandtab;indentwidth=4' global toggle-buffer '@'       ': expandtab;set buffer indentwidth 4;set buffer softtabstop 4;pop-tabs-info expandtab<ret>'
map -docstring 'expandtab;indentwidth=2' global toggle-buffer '<space>' ': expandtab;set buffer indentwidth 2;set buffer softtabstop 2;pop-tabs-info expandtab<ret>'
map -docstring 'noexpandtab'             global toggle-buffer '<tab>'   ': noexpandtab;unset buffer indentwidth;unset buffer softtabstop;pop-tabs-info noexpandtab<ret>'
map -docstring 'smarttab'                global toggle-buffer '<a-tab>' ': smarttab;unset buffer indentwidth;unset buffer softtabstop;pop-tabs-info smarttab<ret>'
map -docstring 'set tabstop (b)'         global toggle-buffer 'p'       ': set buffer tabstop '
map -docstring 'set indentwidth (b)'     global toggle-buffer 'i'       ': set buffer indentwidth '
map -docstring 'set softtabstop (p)'     global toggle-buffer 's'       ': set buffer softtabstop '
define-command -hidden pop-tabs-info -params 1 %{
info -title "%arg{1}" \
"tabstop (b):     %opt{tabstop}
indentwidth (b): %opt{indentwidth}
softtabstop (p): %opt{softtabstop}
"
}

declare-user-mode toggle-numbers
map -docstring 'relative numbers' global toggle-numbers '=' ' switch-numbers -relative<ret>'
map -docstring 'normal numbers'   global toggle-numbers 'n' ': switch-numbers<ret>'
map -docstring 'drop numbers'     global toggle-numbers 'd' ': numbers-off<ret>'

declare-user-mode toggle-colors
map -docstring 'edit colors'         global toggle-colors e %{: e %val{config}<a-!>/colors/}
map -docstring 'colorscheme browser' global toggle-colors b ': colorscheme-browser<ret>'
map -docstring 'palette enable'      global toggle-colors p ': palette-enable<ret>'
map -docstring 'palette disable'     global toggle-colors <a-p> ': palette-disable<ret>'

## System clipboard
declare-user-mode clipboard
declare-option -hidden str system_clipboard_copy
declare-option -hidden str system_clipboard_paste
evaluate-commands %sh{
    case $(uname) in
        Linux) copy="xclip -i"; paste="xclip -o" ;;
        Darwin) copy="pbcopy"; paste="pbpaste" ;;
    esac
    printf "map global clipboard -docstring 'yank to system clipboard' y '<a-|>%s<ret>: echo -markup %%{{Information}selection copied to system clipboard}<ret>'\n" "$copy"
    printf "map global clipboard -docstring 'paste (after) from system clipboard' p '<a-!>%s<ret>'\n" "$paste"
    printf "map global clipboard -docstring 'paste (before) from system clipboard' P '!%s<ret>'\n" "$paste"
    printf "map global clipboard -docstring 'replace from system clipboard' R '|%s<ret>'\n" "$paste"
    printf "set-option global system_clipboard_copy '%s'\n" "$copy"
    printf "set-option global system_clipboard_paste '%s'\n" "$paste"
}
map -docstring 'tmux-clipboard' global clipboard t ': enter-user-mode tmux-clipboard<ret>'
map -docstring 'dump'           global clipboard d %{: echo -to-file '%sh(dirname "$kak_buffile")<a-!>/dquote.dump' -- %reg{dquote}}
# https://discuss.kakoune.com/t/comment-line-s-and-paste-uncommented/1049
map -docstring 'comment and paste' global clipboard '#' \
%{: exec -save-regs '"' <lt>a-s>gixy:<lt>space>comment-line<lt>ret><lt>space><lt>a-p><lt>a-_><ret>}

declare-user-mode tmux-clipboard
map -docstring 'paste (after)'          global tmux-clipboard p '<a-!>tmux showb<ret>'
map -docstring 'paste (before)'         global tmux-clipboard P '!tmux showb<ret>'
map -docstring 'replace selection'      global tmux-clipboard R '|tmux showb<ret>'
map -docstring 'choose-buffer to " reg' global tmux-clipboard m ': tmux-choose-buffer<ret>'
map -docstring 'yank to !kak buffer'    global tmux-clipboard y %{<a-|>tmux setb -- "$kak_selection"<ret>}
map -docstring 'opt to buffer'          global tmux-clipboard o %{<a-|>tmux setb -b kak -- "$kak_opt_"<left>}
map -docstring 'val to buffer'          global tmux-clipboard v %{<a-|>tmux setb -b kak -- "$kak_"<left>}
map -docstring 'dump kak buffer'        global tmux-clipboard d %{<a-|>tmux saveb -b kak '%sh(dirname "$kak_buffile")<a-!>/'<left>}
map -docstring 'dump arbitrary buffer'  global tmux-clipboard <a-d> %{<a-|>tmux choose-buffer "saveb -b '%%%%' '%sh(dirname ""$kak_buffile"")<a-!>/tmux-buf'"}

declare-user-mode anchor
map -docstring 'slice by word'          global anchor ,       ': slice-by-word<ret>'
map -docstring 'flip cursor and anchor' global anchor .       '<a-;>'
map -docstring 'selection hull'         global anchor <space> ': selection-hull<ret>'
map -docstring 'anchor after cursor'    global anchor h       '<a-:><a-;>'
map -docstring 'cursor after anchor'    global anchor u       '<a-:>'
map -docstring 'split to edges'         global anchor s       '<a-S>'
map -docstring 'decr sides'             global anchor m       '<a-:>H<a-;>L<a-;>'
map -docstring 'incr sides'             global anchor <a-m>   '<a-:>L<a-;>H<a-;>'

declare-user-mode vv-np
map -docstring 'next selection' global vv-np ']' '<esc>nvv<ret>'
map -docstring 'prev selection' global vv-np '[' '<esc><a-n>vv<ret>'

declare-user-mode word-case
map -docstring '(lock)'    global word-case . ': enter-user-mode -lock word-case<ret>'
map -docstring 'camelcase' global word-case c ': camelcase<ret>'
map -docstring 'snakecase' global word-case s ': snakecase<ret>'
map -docstring 'kebabcase' global word-case k ': kebabcase<ret>'

declare-user-mode echo-mode
map -docstring 'opt'                  global echo-mode o ':echo %opt{}<left>'
map -docstring 'opt debug'            global echo-mode O ':echo -debug %opt{}<left>'
map -docstring 'val'                  global echo-mode v ':echo %val{}<left>'
map -docstring 'val debug'            global echo-mode V ':echo -debug %val{}<left>'
map -docstring 'to file'              global echo-mode f ':echo -to-file '
map -docstring 'reg'                  global echo-mode r ':echo %reg{}<left>'
map -docstring 'reg debug'            global echo-mode R ':echo -debug %reg{}<left>'
map -docstring 'sh'                   global echo-mode s ':echo %sh{}<left>'
map -docstring 'sh debug'             global echo-mode S ':echo -debug %sh{}<left>'
map -docstring 'ModeChange debug on'  global echo-mode m ': hook -group echo-mode window ModeChange .* %{ echo -debug ModeChange %val{hook_param} }<ret>'
map -docstring 'ModeChange debug off' global echo-mode M ': rmhooks window echo-mode<ret>'
map -docstring 'set debug options'    global echo-mode d ': set window debug '
# map -docstring 'set @ reg'            global echo-mode '@' ':set-register @ ''''<left>'

# declare-user-mode tig
# map global tig h -docstring "tig buffile history" \
# %{: connect-terminal tig %val{bufname}<ret>}
# map global tig b -docstring "tig blame" \
# %{: tmux-terminal-window tig blame "+%val{cursor_line}" -- %val{bufname}<ret>}
# map global tig <space> -docstring "tig status (for committing)" \
# %{: connect-terminal tig status<ret>}

declare-user-mode git
map global git <tab> -docstring 'toggle blame gutter' \
': git-toggle-blame<ret>'
map global git g -docstring 'tig: explore buffer history' \
': + tig -- "%val{buffile}"<ret>'
map global git G -docstring 'tig: explore repository history' \
': + tig<ret>'
map global git b -docstring 'tig: blame' \
': + tig blame "+%val{cursor_line}" -- "%val{buffile}"<ret>'
map global git u -docstring 'tig: show the working tree status' \
': + tig status<ret>'
map global git U -docstring 'tig: show the working tree status (Z)' \
': + tig status<ret>,wz'
map global git c -docstring 'commit: record changes to the repository' \
': git commit<ret>'
map global git d -docstring 'diff: show changes between HEAD and working tree' \
': git diff<ret>'
map global git D -docstring 'diff: show staged (cached) changes' \
': git diff --staged<ret>'
map global git h -docstring 'github: copy canonical GitHub URL' \
': github-url<ret>'
map global git o -docstring 'log all commits for the selection' \
': git-log-lines<ret>'
map global git ? -docstring 'show blamed commit for the selection' \
': git-show-blamed-commit<ret>'
map global git w -docstring 'write: write and stage the current file' \
': write;git add<ret>'
map global git s -docstring 'git status' \
': git status -bs<ret>'
map global git <]> -docstring 'next-hunk' \
'<esc>: git next-hunk;execute-keys vv;enter-user-mode git<ret>'
map global git <[> -docstring 'prev-hunk' \
'<esc>: git prev-hunk;execute-keys vv;enter-user-mode git<ret>'
map global git <minus> -docstring 'broot git status' \
%{: broot -gc :gs<ret>}

declare-user-mode tmux-window
map global tmux-window h -docstring 'Horizontal' \
': terminal-set buffer tmux tmux-terminal-horizontal tmux-focus;new unalias buffer terminal<ret>'
map global tmux-window v -docstring 'Vertical' \
': terminal-set buffer tmux tmux-terminal-vertical tmux-focus;new unalias buffer terminal<ret>'
map global tmux-window w -docstring 'Window' \
': terminal-set buffer tmux tmux-terminal-window tmux-focus;new unalias buffer terminal<ret>'
map global tmux-window o %{: nop %sh{tmux last-pane -Z}<ret>} -docstring 'Last window'
map global tmux-window O %{: nop %sh{tmux kill-pane -a}<ret>} -docstring 'Only window'
map global tmux-window e %{: nop %sh{tmux resize-mode}<ret>} -docstring 'Resize mode'
map global tmux-window z %{: nop %sh{tmux resize-pane -Z}<ret>} -docstring 'Zoom window'
map global tmux-window q ': q<ret>' -docstring 'quit'

declare-user-mode lang-mode

declare-user-mode syntax-tree
map global syntax-tree <space> '<esc>: enter-user-mode -lock syntax-tree<ret>' -docstring '(lock)'
map global syntax-tree ')' ': tree-select-next-node<ret>' -docstring 'select next'
map global syntax-tree '(' ': tree-select-previous-node<ret>' -docstring 'select previous'
map global syntax-tree '&' ': tree-select-children<ret>' -docstring 'select children'
map global syntax-tree '=' ': tree-select-parent-node<ret>' -docstring 'select parent'
map global syntax-tree '*' ': tree-select-first-child<ret>' -docstring 'select first child'
map global syntax-tree '#' ': tree-node-sexp<ret>' -docstring 'tree node sexp'

# kakoune-cd ┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈

map global cd o '<esc>: print-working-directory<ret>' -docstring 'print working dir'
map global cd u '<esc>: change-directory-current-buffer<ret>' -docstring 'current buffer dir'
# map global cd <minus> '<esc>: edit-current-buffer-directory<ret>' -docstring 'edit in current buffer dir'
# unmap global cd e
unmap global cd b

# kakoune-buffers ┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈┈

unmap global buffers n
unmap global buffers p
unmap global buffers u
unmap global buffers D
map global buffers a 'ga: info-buffers<ret>'                -docstring '↔ alternate'
map global buffers h ': buffer-first;info-buffers<ret>'     -docstring '⇐ first'
map global buffers l ': buffer-last;info-buffers<ret>'      -docstring '⇒ last'
map global buffers d ': buffer *debug*<ret>'                -docstring '*debug*'
map global buffers r ': rename-buffer %val{bufname}<a-!>'   -docstring 'rename {…}'
map global buffers t ': format-buffer<ret>'                 -docstring 'format buffer'
map global buffers @ ': trailing-whitespace-del<ret>'       -docstring 'delete trailing whitespace'
map global buffers [ ': buffer-previous;info-buffers<ret>'  -docstring '← previous'
map global buffers ] ': buffer-next;info-buffers<ret>'      -docstring '→ next'
map global buffers . ': enter-user-mode -lock buffers<ret>' -docstring '(lock)'
map global buffers <semicolon> ': pick-buffers<ret>'        -docstring 'pick buffer'
map global buffers <space> ': e!<ret>'                      -docstring 'reload buffer'
map global buffers <backspace> ': delete-buffer<ret>'       -docstring 'delete'
map global buffers <a-backspace> ': delete-buffers<ret>'    -docstring 'delete all'
map global buffers <ret> ': w<ret>'                         -docstring 'write'
map global buffers <a-ret> ': mkdir;w<ret>'                 -docstring 'mkdir and write'

